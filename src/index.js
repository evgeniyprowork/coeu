import './scss/main.scss';


import sliders from './modules/sliders';


// import scrollbar from './modules/scrollbar';


import uploadCheck from './modules/upload';

import picture from './modules/picture';


'use strict';
// YOUR JS code HERE


/* JS Start when Document Load & DOM build*/
document.addEventListener('DOMContentLoaded', ready);
function ready() {


    


    if (window.matchMedia("(min-width: 993px)").matches) {
        picture();
        sliders();
        uploadCheck();
        // scrollbar();
    } else {
        sliders();
        uploadCheck();
        // scrollbar();
    }

}


/* JS Start when Document Load & DOM build & All Scripts and Images Load*/
window.onload = function() {

};

/* ==========================================================================
* jQuery Scripts
* ========================================================================== */
