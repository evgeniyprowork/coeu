const picture = function () {

    const imgScaling = () => {
        const modal = document.querySelector('.modal');
        const img = document.querySelector('.certificate__main-img');
        const span = document.querySelector('.close');


        if (img) {
            img.addEventListener( 'click', () => {
                modal.classList.add('active');
            });

            span.addEventListener( 'click', () => {
                modal.classList.remove('active');
            });

        }


    }

    imgScaling();






}


export default picture;