import Swiper from "swiper";
import 'swiper/swiper-bundle.css';
import SwiperCore, { Navigation, Pagination } from 'swiper/core';

SwiperCore.use([Navigation, Pagination]);




const sliders = function() {

    // const swiperPrev = document.getElementById('swiperPrev')
    // const swiperNext = document.getElementById('swiperNext')
    //
    // swiperPrev.addEventListener('click', () => {
    //     swiperSliderTestimonials.slidePrev();
    // })
    // swiperNext.addEventListener('click', () => {
    //     swiperSliderTestimonials.slideNext();
    // })

    const swiperSliderTestimonials = new Swiper('.exp__swiper-container', {
        // wrapperClass: 'exp__list',
        // slideClass: 'exp__elem',
        slidesPerView: 4,
        spaceBetween: 30,
        grabCursor: true,
        // allowTouchMove: false,
        // effect: 'fade',
        // centeredSlides: true,
        // loop: true,
        // slidesOffsetAfter: 0,
        watchOverflow: true,
        resistanceRatio: 0,
        navigation: {
            nextEl: '.exp__next',
            prevEl: '.exp__prev',
        },
        breakpoints: {
            1300: {
                slidesPerView: 4,
            },
            992: {
                slidesPerView: 3,
                spaceBetween: 20,
            },
            576: {
                slidesPerView: 2,
                spaceBetween: 15
            },
            320: {
                slidesPerView: 1
            }
        }


    });

    const swiper = new Swiper('.statistics__container', {
        wrapperClass: 'statistics__list',
        slideClass: 'statistics__elem',
        slidesPerView: 1,
        spaceBetween: 10,
        grabCursor: true,
        // allowTouchMove: false,
        // effect: 'fade',
        // centeredSlides: true,
        // loop: true,
        // slidesOffsetAfter: 0,
        watchOverflow: true,
        resistanceRatio: 0,
        navigation: {
            nextEl: '.statistics__next',
            prevEl: '.statistics__prev',
        },
        breakpoints: {
            1200: {
                slidesPerView: 2,
            },
            769: {
                slidesPerView: 1,
            },
            577: {
                slidesPerView: 2,
            },

        }


    });

    const swiperStatistics = new Swiper('.works__swiper-container', {
        wrapperClass: 'works__list',
        slideClass: 'works__elem',
        slidesPerView: 1,
        spaceBetween: 30,
        grabCursor: true,
        // allowTouchMove: false,
        // effect: 'fade',
        // centeredSlides: true,
        // loop: true,
        // slidesOffsetAfter: 0,
        watchOverflow: true,
        resistanceRatio: 0,
        navigation: {
            nextEl: '.works__next',
            prevEl: '.works__prev',
        },
        breakpoints: {
            1441: {
                slidesPerView: 4,
            },
            993: {
                slidesPerView: 3,
            },
            421: {
                slidesPerView: 2,
            }
        }


    });


}

export default sliders;