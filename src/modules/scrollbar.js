import PerfectScrollbar from 'perfect-scrollbar';
import 'perfect-scrollbar/css/perfect-scrollbar.css';

const scrollbar = function () {

    const ps = new PerfectScrollbar('#container', {
        wheelSpeed: 2,
        wheelPropagation: true,
        minScrollbarLength: 20
    });

    ps.update();
}

export default scrollbar;
